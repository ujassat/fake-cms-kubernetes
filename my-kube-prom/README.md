# Monitoring
The monitoring stack is done through [kube-prometheus](https://github.com/coreos/kube-prometheus) and uses jsonnet, a templating language for json.
More information can be found on the kube-prometheus [github](https://github.com/coreos/kube-prometheus).

## Deploy the monitoring stack
To get started quickly
First add the configmap for the blackbox exporter and object store secret for thanos sidecar
```
$ kubectl -n monitoring create configmap blackbox-config --from-file=configs/blackbox.yml
$ kubectl -n monitoring create secret generic thanos-objstore-config --from-file=thanos.yaml=configs/thanos-config.yaml
``` 

Then create the monitoring stack
```
$ kubectl create -f manifests/

# It can take a few seconds for the above 'create manifests' command to fully create the following resources, so verify the resources are ready before proceeding.
$ until kubectl get customresourcedefinitions servicemonitors.monitoring.coreos.com ; do date; sleep 1; echo ""; done
$ until kubectl get servicemonitors --all-namespaces ; do date; sleep 1; echo ""; done

$ kubectl apply -f manifests/ # This command sometimes may need to be done twice (to workaround a race condition).
```

## Tear down stack

```
$ kubectl delete -f manifests/
```

## Compiling customized manifests
### Dependencies
* Jsonnet
```
$ go get github.com/google/go-jsonnet/cmd/jsonnet
```

* gojsontoyaml
```
$ go get github.com/brancz/gojsontoyaml
```
## Compiling
The included build script can be used 

```
$ ./build.sh example.jsonnet
```

# Notes
This using a custom version of thanos v0.5.0 that is available as a docker image from the registry at
```
gitlab-registry.cern.ch/ujassat/fake-cms-kubernetes/thanos
```
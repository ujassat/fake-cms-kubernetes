{
  _config+:: {
    imageRepos+:: {
      thanos: 'vm-master:443/thanos',
      prometheus: 'vm-master:443/prometheus',
      nodeExporter: 'vm-master:443/node-exporter',
      alertmanager: 'vm-master:443/alertmanager',
      configmapReloader: 'vm-master:443/configmap-reload',
      kubeRbacProxy: 'vm-master:443/kube-rbac-proxy',
      prometheusOperator: 'vm-master:443/prometheus-operator',
      prometheusConfigReloader: 'vm-master:443/prometheus-config-reloader',
      prometheusAdapter: 'vm-master:443/k8s-prometheus-adapter-amd64',
      kubeStateMetrics: 'vm-master:443/kube-state-metrics',
    },
  },
}
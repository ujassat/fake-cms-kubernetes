local k = import 'ksonnet/ksonnet.beta.3/k.libsonnet';

local kp =
  (import 'kube-prometheus/kube-prometheus.libsonnet') +
  // Uncomment the following imports to enable its patches
  // (import 'kube-prometheus/kube-prometheus-anti-affinity.libsonnet') +
  // (import 'kube-prometheus/kube-prometheus-managed-cluster.libsonnet') +
  (import 'kube-prometheus/kube-prometheus-node-ports.libsonnet') +
  // (import 'kube-prometheus/kube-prometheus-static-etcd.libsonnet') +
  (import 'kube-prometheus/kube-prometheus-thanos-sidecar.libsonnet') +
  // (import 'registry-images.jsonnet') // Use the images from the private registry, requires all images to be there
  {
    _config+:: {
      imageRepos+:: {
        thanos: 'vm-master:443/thanos',
      },
      namespace: 'monitoring',
    },
  prometheus+:: {
    sqlServiceMonitor: {
      apiVersion: 'monitoring.coreos.com/v1',
      kind: 'ServiceMonitor',
      metadata: {
        name: 'sql-exporter',
        namespace: 'default',
      },
      spec: {
        selector: {
          matchLabels: {
            app: 'sql-exporter',
          },
        },
        endpoints: [
          {
            port: 'http',
            interval: '15s',
          },
        ],
      },
    },
    swatchCellServiceMonitor: {
      apiVersion: 'monitoring.coreos.com/v1',
      kind: 'ServiceMonitor',
      metadata: {
        name: 'swatch-cell',
        namespace: 'default',
      },
      spec: {
        selector: {
          matchLabels: {
            app: 'swatch-cell',
          },
        },
        endpoints: [
          {
            port: 'metrics',
            interval: '15s',
          },
        ],
      },
    },
    blackboxExporterServiceMonitor: {
      apiVersion: 'monitoring.coreos.com/v1',
      kind: 'ServiceMonitor',
      metadata: {
        name: 'blackbox-exporter',
        namespace: $._config.namespace,
      },
      spec: {
        selector: {
          matchLabels: {
            app: 'blackbox-exporter',
          },
        },
        endpoints: [
          {
            port: 'http',
            interval: '15s',
          },
        ],
      },
    },
    blackboxExporterService:
      local service = k.core.v1.service;
      local servicePort = k.core.v1.service.mixin.spec.portsType;

      local blackboxExporterServiceNodePort = servicePort.newNamed('http', 9115, 'http');

      service.new('blackbox-exporter', $.prometheus.blackboxExporterDeployment.spec.selector.matchLabels, blackboxExporterServiceNodePort) +
      service.mixin.metadata.withLabels({ app: 'blackbox-exporter' }) +
      service.mixin.metadata.withNamespace($._config.namespace),
    blackboxExporterDeployment: 
      local deployment = k.apps.v1beta2.deployment;
      local container = k.apps.v1beta2.deployment.mixin.spec.template.spec.containersType;
      
      local containerPort = container.portsType;

      local portName = 'http';
      local targetPort = 9115; 

      local podLabels = { app: 'blackbox-exporter' };

      local volume = k.apps.v1beta2.deployment.mixin.spec.template.spec.volumesType;
      local containerVolumeMount = container.volumeMountsType;

      local configVolumeMountLocation = '/etc/config';
      local configVolumeName = 'blackbox-config';
      local configVolume = volume.withName(configVolumeName) + volume.mixin.configMap.withName(configVolumeName);
      local configVolumeMount = containerVolumeMount.new(configVolumeName, configVolumeMountLocation);

      local c = container.new('blackbox-exporter', 'prom/blackbox-exporter:master') +
      container.withPorts(containerPort.newNamed(portName, targetPort)) +
      container.withVolumeMounts([configVolumeMount],) +
      container.withArgs([
        '--config.file',
        configVolumeMountLocation + '/blackbox.yml',
        '--web.listen-address=:' + targetPort
      ]);

      deployment.new('blackbox-exporter', 1, c, podLabels) +
      deployment.mixin.metadata.withNamespace($._config.namespace) +
      deployment.mixin.spec.selector.withMatchLabels(podLabels) + 
      deployment.mixin.spec.template.spec.withVolumes([configVolume],),
  },
};

{ ['00namespace-' + name]: kp.kubePrometheus[name] for name in std.objectFields(kp.kubePrometheus) } +
{ ['0prometheus-operator-' + name]: kp.prometheusOperator[name] for name in std.objectFields(kp.prometheusOperator) } +
{ ['node-exporter-' + name]: kp.nodeExporter[name] for name in std.objectFields(kp.nodeExporter) } +
{ ['kube-state-metrics-' + name]: kp.kubeStateMetrics[name] for name in std.objectFields(kp.kubeStateMetrics) } +
{ ['alertmanager-' + name]: kp.alertmanager[name] for name in std.objectFields(kp.alertmanager) } +
{ ['prometheus-' + name]: kp.prometheus[name] for name in std.objectFields(kp.prometheus) } +
{ ['prometheus-adapter-' + name]: kp.prometheusAdapter[name] for name in std.objectFields(kp.prometheusAdapter) } +
{ ['grafana-' + name]: kp.grafana[name] for name in std.objectFields(kp.grafana) }

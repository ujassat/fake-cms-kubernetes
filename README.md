# Fake CMS Kubernetes setup

These instructions can be used to set up a kubernetes cluster to work within an network environment which only has an external SSH connection. 

The setup includes a single master node with as many slaves as is neccesary. Authentication is done through a webhook, which runs on the same machine as the master, and authorization through RBAC.
To allow for easier deployments a docker registry is set up on the master which is used by the slaves to pull images.

##  Machine setup
This sets up machines to run docker and kubernetes, the script contains assumes access to the internet.
To set up kubernetes on a machine without an internet connection instruction can be used from [here](https://ahmermansoor.blogspot.com/2019/04/install-kubernetes-k8s-offline-on-centos-7.html).

This needs to be run on both the masters and the slaves.

```
$ chmod +x init_cc7.sh
$ sudo ./init_cc7.sh
```

## Kubernetes cluster set up

``kubeadm`` is used to set up the kubernetes cluster and has been tested to work with both the ```calico``` and ```funnel``` pod networks.

### Master Machine
```
$ sudo kubeadm init --pod-network-cidr=10.244.0.0/16
```

When ``kubeadm`` has finished three commands will be printed that can be used to set up ``kubectl``. 
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

A join command will also be printed, this will be needed to connect the slaves.
```
kubeadm join <master-ip>:<master-port> --token <token> --discovery-token-ca-cert-hash sha256:<hash> 
```

To apply the pod network run
```
$ kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/62e44c867a2846fefb68bd5f178daf4da3095ccb/Documentation/kube-flannel.yml
```

### Slave Machines
Simply run the join command provided by ``kubeadm init`` on the master
```
$ kubeadm join <master-ip>:<master-port> --token <token> --discovery-token-ca-cert-hash sha256:<hash> 
``` 

## Docker registry setup
The docker registry used is set up inside a container on the master machine. In order for this to be accessed by the other machines the registry needs to be run on https, which requires a certificate. In this case a self-signed certificate is used and this certificate is then installed on the master and slave machines.

First create the certificate:
```
$ mkdir -p certs
$ openssl req -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key -x509 -days 365 -out certs/domain.crt
```
Make sure to enter the host name or address of the master machine under "Common Name" when creating the certificate.

Use the certificate to to make the registry:
```
$ docker run -d \
  --restart=always \
  --name registry \
  -v "$(pwd)"/certs:/certs \
  -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
  -p 443:443 \
  registry:2
```
This will run a docker registry on port 443 of the master machine.

Then the certificate can be installed on all the master and slave machines by copying  ```domain.crt``` to ```/etc/docker/certs.d/<master host ip_address/name>:443/ca.crt```
```
$ mkdir -p /etc/docker/certs.d/<master host ip_address/name>:443
$ cp certs/domain.crt /etc/docker/certs.d/<master host ip_address/name>:443/ca.crt
```

### Loading images into the repository
* Save a created image as a tar file
```
$ docker save -o <path for generated tar file> <image name>
```

* Copy the tar file across to the master using scp

* Load the image from the tar file
```
$ docker load -i <path to image tar file>
```

* Tag and push the image
```
$ docker tag <image name> <hostname/ipaddress>:443/<image name>
$ docker push <hostname/ipaddress>:443/<image name>
```

## Webhook authentication
This process will set up a simple webhoook process run by a python script on the same master machine.
It will uses the same certificates as the docker registry

* Edit webhook_config.yml to point to the master hostname/ip and port (default 5000 with the python script provided)
```
...
      server: https://<hostame/ip>:<port>/authenticate # URL of remote service to query. Must use 'https'.
...
```
* Copy the edited file and certificates to /etc/kubernetes/auth/
```
$ sudo mkdir -p /etc/kubernetes/auth
$ sudo -r cp certs/ webhook_config.yml /etc/kubernetes/auth/
```
* Edit the api server manifest to mount the needed files in the api-server image
```
$ sudo sed -i '/volumeMounts:/ a \ \ \ \ - mountPath: /etc/kubernetes/auth\n      name: auth\n      readOnly: true' /etc/kubernetes/manifests/kube-apiserver.yaml
$ sudo sed -i '/volumes:/ a \ \ - hostPath:\n      path: /etc/kubernetes/auth\n      type: DirectoryOrCreate\n    name: auth' /etc/kubernetes/manifests/kube-apiserver.yaml
```
* Edit the api server manifest telling it to use the webhook authentication made
```
$ sudo sed -i '/- kube-apiserver/ a \ \ \ \ - --authentication-token-webhook-config-file=/etc/kubernetes/auth/webhook_config.yml' /etc/kubernetes/manifests/kube-apiserver.yaml
``` 

## Namespacing nodes
The following steps will allow the nodes to act as if they are in a namespace by deploying all pods within a certain namespace onto a specific node(s).
* Enable PodNodeSelector plugin
```
$ sed -i 's/--enable-admission-plugins.*/&,PodNodeSelector/' /etc/kubernetes/manifests/kube-apiserver.yaml
```
* Apply namespaces with an annotation of the form:
```
annotations:
  scheduler.alpha.kubernetes.io/node-selector: ns=<namespace_name>
```
This annotation will be used to determine which nodes to run the pods on.
``namepace.yml`` is an example namespace file

* Label each of the nodes with the appropiate namespace labels
```
kubectl label node <node> ns=<namespace_name>
```

## Rolebindings
Apply RBAC rolebindings to restrict users to certain namespace and permissions using Roles and RoleBindings. An example is provided in `rolebindings.yml`
To read more about RBAC in kubernetes look at the [documentation](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)

local k = import 'ksonnet/ksonnet.beta.4/k.libsonnet';
local sts = k.apps.v1.statefulSet;
local deployment = k.apps.v1.deployment;
local service = k.core.v1.service;
local servicePort = k.core.v1.service.mixin.spec.portsType;

local kt =
  (import 'kube-thanos/kube-thanos-querier.libsonnet') +
  //(import 'kube-thanos/kube-thanos-store.libsonnet') +
  // (import 'kube-thanos/kube-thanos-pvc.libsonnet') + // Uncomment this line to enable PVCs
  // (import 'kube-thanos/kube-thanos-receive.libsonnet') +
  // (import 'kube-thanos/kube-thanos-sidecar.libsonnet') +
  // (import 'kube-thanos/kube-thanos-servicemonitors.libsonnet') +
  {
    thanos+:: {
      variables+:: {
        image: 'vm-master:443/thanos:v0.5.0',
        objectStorageConfig+: {
          name: 'thanos-objstore-config',
          key: 'thanos.yaml',
        },
      },

      querier+: {
        deployment+:
          deployment.mixin.spec.withReplicas(2),
          service+:
            service.mixin.spec.withPorts([servicePort.newNamed('http', 9090, 'http') + servicePort.withNodePort(30909), servicePort.newNamed('grpc', 10901, 'grpc') + servicePort.withNodePort(30910)]) +
            service.mixin.spec.withType('NodePort'),
      },
      store+: {
        statefulSet+:
          sts.mixin.spec.withReplicas(5),
      },
    },
  };

{ ['thanos-querier-' + name]: kt.thanos.querier[name] for name in std.objectFields(kt.thanos.querier) } 
// { ['thanos-store-' + name]: kt.thanos.store[name] for name in std.objectFields(kt.thanos.store) }
// { ['thanos-receive-' + name]: kt.thanos.receive[name] for name in std.objectFields(kt.thanos.receive) }



